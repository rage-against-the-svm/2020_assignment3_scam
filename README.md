SCAM
----
## Introduction
### Assignment information
Project title: Simple Crud Arrangement Manager

Repository: https://gitlab.com/rage-against-the-svm/2020_assignment3_scam

Group members:
* Alind Xhyra 829865
* Mario Marino 829707
* Pietro Tropeano 829757
* Davide Pietrasanta 844824

### How to Install and Execute

#### Docker

```
docker build -t scam
docker run -p 8080:8080 scam
```

#### Alternative, with Java 11

```
./mvnw spring-boot:run
```

#### After the istallation

After the installation open your browser and go to: http://localhost:8080


## Project information

The application is born for a Business-to-business environment of large companies where you need to keep track of which employees are elected administrators for the various commissioned services. By tracking additional information such as topics processed within the service, it is possible to customize the advertisement contracts that the company offers.

### Project structure description

![alt text](ER.png)


We used bidirectional single table pattern for One-To-Many relationships to simplify data access.
There are four main classes: Admin, Service, App and Topic.

### Admin
This class represents the employees that the company elected as administrator of a project.
The company tracks the name, the vat number and the mail of the employee.
Each employee can manage and handle many different services.

### Service
This class represents the services built and offered by the company.
For each service a link, the average number of users, the name and the hosting platform are tracked.
A service is managed from an administrator.
A service is labeled with many different topics, this system allows quick access to the target of the service and customize the service advertisement contracts offered by the company.

### App
This class is a specialized service used to represent a mobile application.
A mobile application is useful to track the number of devices which installed the app and the compatibility of the application with the different mobile operating systems.

### Topic
This class represents a topic, for example “Space exploration” or “moon landing”.
Each topic has got a name and a description.
A topic can be used to label many services.
A topic can be a subtopic of another one, for example “moon landing” is a subtopic of “Space exploration”, to track this information we say “space exploration” include “moon landing”.

# Responsibilities
The project uses a Model View Controller (MVC) pattern with a Service layer for each class. The Unit of work is managed by Hibernate and synchronize instantly the changes. The Repository pattern is a strategy for abstracting data access allowing more focus on the business logic.

We follow `Page controller` Design Pattern so there is a controller for each page (class).
Controllers define the application behavior and the business logic.
Controllers selects and returns the right view formatted with the right data.
Each controller implements the logic of the CRUD operations for its class.

There is a model for each class.
Models are used to encapsulate the state of the application.
Models are implemented using Plain Old Java Objects (POJO's)
Each model is responsible for its class.

We follow `Template View` Design Pattern so there is some static content (in the html files) and some dynamic content (in View classes).
There is a view for each class.
Views are used to render the models.
Views are used to allow users to use the CRUD functionalities of the application.

There is a Service for each class.
Each Service is specialized from the ScamService Generics interface.
Services are thought to be implemented as being part of a large system and are used to handle business logic.

