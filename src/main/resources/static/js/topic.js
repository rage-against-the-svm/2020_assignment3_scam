let topicData = "";
let network = "";
let nodes = "";
let edges = "";
$(document).ready(function(){
    $("#searchTopics").bsMultiSelect();
    $("#addBtn").click(function() {
        $("#addForm").toggle();
    });
    $("#itemsEditTable").hide();
    $("#nameOpt").bsMultiSelect();
    drawGraph();
});
function drawGraph(){
    $.ajax({
        type:'GET',
        url:"/topic/graph",
        success:function(data){
            topicData = JSON.parse(data);
            nodes = new vis.DataSet(topicData['nodes']);
            edges = new vis.DataSet(topicData['edges']);
            let container = document.getElementById('myNetwork');
            let options = {
                edges:{
                    arrows: 'to',
                    color: 'blue',
                    font: '12px arial #ff0000',
                    scaling:{
                        label: true,
                    },
                    shadow: true,
                    smooth: true,
                }
            }
            network = new vis.Network(container, {nodes: nodes,edges: edges}, options);
            network.on('click', function(properties) {
                $("#addForm").hide();
                let id = null;
                if(properties.nodes.length!=0) {
                    id = properties.nodes[0];
                    displayEditTable(id);
                } else {
                    //click a vuoto nel grafo
                    cancelEditFnc('topic',id);
                }
            });
        }
    });
}
function displayEditTable(id){
    let nodeName = nodes.get(id)["label"];
    let nodeDescription = topicData["description"][id];
    let nodeTopics = ""
    topicData["parents"][id].forEach(function (item) {
        nodeTopics = nodeTopics + nodes.get(item)["label"] + ", ";
    })
    nodeTopics = nodeTopics.replace(/,\s*$/, "");
    $('#editRow').remove();
    $('#itemRow').remove();
    let deleteAndEdit =
        '<td>' +
        '<a class="btn btn-primary" style="color:white" onclick="delFnc(\'topic\','+id+')" class="btnTh">Delete</a>' +
        '</td>' +
        '<td>' +
        '<a class="btn btn-primary" style="color:white" onclick="toggleEdit(\'topic\','+id+')" class="btnTh">Edit</a>' +
        '</td>';
    let row =
        '<tr id="itemRow">' +
        '<td id="nodeId" class="idTh">'+id+'</td>'+
        '<td id="nodeName" class="nameTh">' + nodeName + '</td>' +
        '<td id="nodeDesc" class="descTh">' + nodeDescription + '</td>' +
        '<td id="nodeTopics" class="parentsTh">' + nodeTopics + '</td>'+
        deleteAndEdit +
        '</tr>'
    $('#itemsEditTable').append($(row));
    $("#itemsEditTable").show();
    $("#topicTableHeader").hide();
    $(".selectedEdit").removeClass("selectedEdit");
    $("#idCell"+id).parent().addClass("selectedEdit");
    $("#nodeId").parent().addClass("selectedEdit");
}
function toggleEditMVC(id){
    displayEditTable(id);
    toggleEdit(id);
}