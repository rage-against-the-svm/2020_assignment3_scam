package it.rage_against_the_svm.scam.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "App not found")
public class AppNotFoundException extends RuntimeException {

    public AppNotFoundException() {
        super();
    }

    public AppNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppNotFoundException(String message) {
        super(message);
    }

    public AppNotFoundException(Throwable cause) {
        super(cause);
    }
}
