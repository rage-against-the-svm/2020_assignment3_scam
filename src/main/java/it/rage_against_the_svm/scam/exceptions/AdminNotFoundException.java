package it.rage_against_the_svm.scam.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Admin not found")
public class AdminNotFoundException extends RuntimeException {

    public AdminNotFoundException() {
        super();
    }

    public AdminNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AdminNotFoundException(String message) {
        super(message);
    }

    public AdminNotFoundException(Throwable cause) {
        super(cause);
    }

}
