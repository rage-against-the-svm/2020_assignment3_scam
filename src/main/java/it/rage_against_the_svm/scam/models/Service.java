package it.rage_against_the_svm.scam.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String link;

    private double userAvg;

    private String name;

    private String hosting;

    @ManyToOne(optional = false)
    @JoinColumn(name = "admin_id", nullable = false)
    private Admin admin;

    @ManyToMany
    @JoinTable(
            name = "LABEL",
            joinColumns = @JoinColumn(name = "SERVICE_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "TOPIC_ID", referencedColumnName = "id")
    )
    private List<Topic> label;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public double getUserAvg() {
        return userAvg;
    }

    public void setUserAvg(double userAvg) {
        this.userAvg = userAvg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHosting() {
        return hosting;
    }

    public void setHosting(String hosting) {
        this.hosting = hosting;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public List<Topic> getLabel() {
        return label;
    }

    public void setLabel(List<Topic> label) {
        this.label = label;
    }
}
