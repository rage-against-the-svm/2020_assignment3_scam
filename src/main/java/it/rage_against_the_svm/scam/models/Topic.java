package it.rage_against_the_svm.scam.models;
import javax.persistence.*;
import java.util.List;

@Entity
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    private String name;

    private String description;

    @ManyToMany
    @JoinTable(
            name = "INCLUDE",
            joinColumns = @JoinColumn(name = "TOPIC_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "SUBTOPIC_ID", referencedColumnName = "id")
    )
    private List<Topic> include;

    @ManyToMany(mappedBy = "label")
    private List<Service> label;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Topic> getInclude() {
        return include;
    }

    public void setInclude(List<Topic> include) {
        this.include = include;
    }

    public List<Service> getLabel() {
        return label;
    }

    public void setLabel(List<Service> label) {
        this.label = label;
    }
}