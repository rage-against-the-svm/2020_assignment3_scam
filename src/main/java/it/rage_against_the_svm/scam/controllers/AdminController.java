package it.rage_against_the_svm.scam.controllers;

import it.rage_against_the_svm.scam.models.Admin;
import it.rage_against_the_svm.scam.services.AdminService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

@Controller
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("/admin")
    public String getAdmins(Model m,
                            @RequestParam(name="name", required = false, defaultValue = "") String name,
                            @RequestParam(name="mail", required = false, defaultValue = "") String mail,
                            @RequestParam(name="vatnumber", required = false, defaultValue = "") String vatnumber) {
        ArrayList<Admin> admins = new ArrayList<>();
        if (name.equals("") && mail.equals("") && vatnumber.equals("")) {
            admins.addAll(adminService.getAll());
        } else {
            admins.addAll(adminService.getAdmins(
                    name,
                    mail,
                    vatnumber));
        }
        System.out.println(name);
        m.addAttribute("admins", admins);
        return "admin";
    }

    @PostMapping("/admin")
    public ResponseEntity<String> addAdmin(@RequestBody Map<String, String> admin) {
        Admin newAdmin = new Admin();
        newAdmin.setEmail(admin.get("mail"));
        newAdmin.setName(admin.get("name"));
        newAdmin.setVatNumber(admin.get("vatin"));

        adminService.create(newAdmin);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/admin/{id}")
    public ResponseEntity<String> deleteAdmin(@PathVariable Integer id) {
        adminService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/admin")
    public ResponseEntity<String> editAdmin(@RequestBody Map<String, String> admin){
        Admin newAdmin = new Admin();
        newAdmin.setName(admin.get("name"));
        newAdmin.setEmail(admin.get("mail"));
        newAdmin.setVatNumber(admin.get("vatin"));

        adminService.update(Integer.parseInt(admin.get("id")), newAdmin);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
