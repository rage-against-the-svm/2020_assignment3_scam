package it.rage_against_the_svm.scam.repositories;

import it.rage_against_the_svm.scam.models.Admin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface AdminRepository extends CrudRepository<Admin, Integer> {
    @Query("select a from Admin a where a.name like %?1% and a.mail like %?2% and a.vatNumber like %?3%")
    List<Admin> findAdmins(String name, String mail, String vatnumber);
}
